import { Component, OnInit } from '@angular/core';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  mensaje : string;

  scanData: {};
  options: BarcodeScannerOptions;

  constructor(private barcodeScanner: BarcodeScanner) {
/*     this.barcodeScanner.scan().then(barcodeData => {
     alert('Barcode data'+ barcodeData);
     this.mensaje = barcodeData.text;
     }).catch(err => {
         console.log('Error', err);
     }); */
   }
  ngOnInit(): void {

  }

  
  scan() {
    this.options = {
      prompt: "Scan your barcode ",
      formats : "PDF_417",
      showFlipCameraButton : true,
      showTorchButton : true,
      orientation	: "landscape"
    };
    this.barcodeScanner.scan(this.options).then(
      (barcodeData) => {
        console.log(barcodeData);
        this.scanData = barcodeData;
      },
      (err) => {
        console.log("Error occured : " + err);
      }
    );
  }
   

}
